using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// a dictionary lookup of sparse chunks of the map
public class SparseChunks {
	public struct ChunkKey{
		public int x;
		public int y;
		public ChunkKey(int _x, int _y){
			x = _x;
			y = _y;
		}
	}
	public int minx, maxx, miny, maxy;
	public int chunkSize = 1;
	public Dictionary<ChunkKey, GameObject[,]> worldMap = new Dictionary<ChunkKey, GameObject[,]>();
	public GameObject GetValue(float x, float y){
		return GetValue ((int)x, (int)y);
	}
	public GameObject GetValue(int x, int y){
		// segments are assumed to be empty until filled
		// translate the position to a chunk key

		ChunkKey ck = new ChunkKey(Mathf.FloorToInt(x / (float)chunkSize), Mathf.FloorToInt(y / (float)chunkSize));
		if (worldMap.ContainsKey(ck)) {
			// offset lookup into segment
			// if ck.x = -1 
			// -1+16
			if(x-ck.x*chunkSize < 0)
				Debug.Log (x + " " + ck.x + " " + (x + (-1*ck.x * chunkSize)));
			return worldMap[ck][x+(-1*ck.x*chunkSize), y-ck.y*chunkSize];
		}
		return null;
	}
	public void SetValue(float x, float y, GameObject val){
		SetValue ((int)x, (int)y, val);
	}
	public void SetValue(int x, int y, GameObject val){
		ChunkKey ck = new ChunkKey(Mathf.FloorToInt(x / (float)chunkSize), Mathf.FloorToInt(y / (float)chunkSize));
		if (!worldMap.ContainsKey(ck)) {
			worldMap.Add(ck, new GameObject[chunkSize,chunkSize]);
		}
		worldMap[ck][Mathf.Abs(x-ck.x*chunkSize), Mathf.Abs(y-ck.y*chunkSize)] = val;
		if (val) {
			minx = Mathf.Min(x, minx);
			miny = Mathf.Min(y, miny);
			maxx = Mathf.Max(x, maxx);
			maxy = Mathf.Max(y, maxy);
		}
	}
	public void Remove(int x, int y){
		ChunkKey ck = new ChunkKey(Mathf.FloorToInt(x / (float)chunkSize), Mathf.FloorToInt(y / (float)chunkSize));
		worldMap.Remove (ck);
	}
	public List<ChunkKey> GetKeys(){
		return new List<ChunkKey>(worldMap.Keys);
	}
}
