using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class LevelTiles {
	public static int version = 1;
	public int _version;

	[XmlArray("tiles"), XmlArrayItem("TileDef")]
	public List<TileDef> tiles = new List<TileDef>();

	public void Save(){
		var serializer = new XmlSerializer(typeof(LevelTiles));
		this._version = version;
		using(var stream = new FileStream(Application.dataPath + "/Levels/Test.xml", FileMode.Create)){
			serializer.Serialize(stream, this);
		}
	}
	public void Add(TileDefC t){
		tiles.Add (t.def);
	}
	public void Remove(TileDefC t){
		tiles.Remove (t.def);
	}

	public static LevelTiles Load()
	{
		string levelPath = Application.dataPath + "/Levels/Test.xml";
		Debug.Log (levelPath);
		if(File.Exists(levelPath)){
			var serializer = new XmlSerializer(typeof(LevelTiles));
			using(var stream = new FileStream(levelPath, FileMode.Open))
			{
				LevelTiles iMap = serializer.Deserialize (stream) as LevelTiles;
				if (iMap._version == null || iMap._version != version) {
					Debug.Log("version mismatch");
					iMap = new LevelTiles ();
					iMap.Save ();
				}
				return iMap;
			}
		}
		Debug.Log ("file doesn't exist");
		return new LevelTiles ();
	}

}

public class TileDef{
	public int x;
	public int y;
	public string spriteName;
	public Color foregroundC, backgroundC;
}
public class TileDefC : MonoBehaviour{
	public TileDef def = new TileDef();
}
