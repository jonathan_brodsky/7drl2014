﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;

public class LevelEditor : MonoBehaviour {
	public List<Sprite> sprites;
	SparseChunks chunks = new SparseChunks();
	public GameObject spriteHolder;
	public GameObject palette;
	Sprite currentSprite;
	Color currentColor;
	bool eraseMode;
	Vector3 lastWorldPos = Vector3.zero;
	// Use this for initialization
	Vector2 centerPixel;
	Texture2D paletteTex;
	LevelTiles levelData;
	Dictionary<string, Sprite> spritesLookup = new Dictionary<string, Sprite>(); 
	void Awake(){
		levelData = LevelTiles.Load();
		// copy across the sprites to the dictionary on load
		foreach (Sprite s in sprites) {
			spritesLookup.Add (s.name, s);
		}
	}
	void Start () {
		foreach (TileDef t in levelData.tiles) {
			if (t.spriteName != null && spritesLookup.ContainsKey (t.spriteName)) {
				GameObject s = (GameObject)Instantiate (spriteHolder, new Vector3 (t.x, t.y, 0), Quaternion.identity);
				s.AddComponent<TileDefC> ();
				s.GetComponent<TileDefC> ().def = t;
				chunks.SetValue (t.x, t.y, s);
				s.GetComponent<SpriteRenderer> ().color = t.foregroundC;
				s.GetComponent<SpriteRenderer> ().sprite = spritesLookup [t.spriteName];
				s.transform.Find ("fill").GetComponent<SpriteRenderer> ().color = t.backgroundC;
			}
		}
		currentSprite = sprites [0];
		paletteTex = (Texture2D)palette.renderer.material.mainTexture;
//		paletteTex = palette.GetComponent<SpriteRenderer> ().sprite.texture;
//		palette.GetComponent<SpriteRenderer> ().
	}
	
	// Update is called once per frame
	void Update () {
		bool dirty = false;
		int result = 0;
		if (Input.inputString.Length > 0 && int.TryParse(Input.inputString, out result)) {
			currentSprite = sprites[Mathf.Min (sprites.Count-1, result-1)];
		}
		if (Input.GetMouseButton (0)) {
			Vector3 worldPos = Camera.main.ScreenToWorldPoint (new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
			worldPos = new Vector3 (Mathf.Round (worldPos.x), Mathf.Round (worldPos.y), 0);
			if (Input.GetMouseButtonDown(0) || worldPos != lastWorldPos) {
				lastWorldPos = worldPos;
				if (!chunks.GetValue (worldPos.x, worldPos.y)) {
					GameObject s = (GameObject)Instantiate (spriteHolder, worldPos, Quaternion.identity);
					s.AddComponent<TileDefC> ();
					s.GetComponent<TileDefC> ().def.x = (int)worldPos.x;
					s.GetComponent<TileDefC> ().def.y = (int)worldPos.y;
					levelData.Add (s.GetComponent<TileDefC> ());
					chunks.SetValue (worldPos.x, worldPos.y, s);
				}
				if (Input.GetKey (KeyCode.E)) {
					GameObject thisSprite = chunks.GetValue (worldPos.x, worldPos.y);
					levelData.Remove(this.GetComponent<TileDefC> ());
					Destroy (thisSprite);
					chunks.Remove ((int)worldPos.x, (int)worldPos.y);
				} else {
					GameObject thisSprite = chunks.GetValue (worldPos.x, worldPos.y);
					thisSprite.GetComponent<TileDefC> ().def.foregroundC = thisSprite.GetComponent<SpriteRenderer> ().color = getColor();
					thisSprite.GetComponent<TileDefC> ().def.backgroundC = thisSprite.transform.Find ("fill").GetComponent<SpriteRenderer> ().color = getColor ();
					thisSprite.GetComponent<SpriteRenderer> ().sprite = currentSprite;
					thisSprite.GetComponent<TileDefC> ().def.spriteName = currentSprite.name;
					thisSprite.transform.eulerAngles = new Vector3 (0, 0, Random.Range (0, 3) * 90);				
				}
				dirty = true;
			}
		}
		if (Input.GetMouseButton (1)) {
			RaycastHit[] hits;
			hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition));
			int i = 0;
			while (i < hits.Length) {
				RaycastHit hit = hits[i];
				if (hit.collider.gameObject == palette) {
					Vector2 pixelUV = hit.textureCoord;
					Debug.Log (pixelUV);
					pixelUV.x *= paletteTex.width;
					pixelUV.y *= paletteTex.height;
					centerPixel = pixelUV;
					Debug.Log (centerPixel);
				}
				i++;
			}
		}
		levelData.Save ();
	}
	Color getColor(){
		return paletteTex.GetPixel ((int)centerPixel.x + Random.Range (-5, 5), (int)centerPixel.y + Random.Range (-5, 5));
	}
}
